package xyz.tytanium.ragnagoblin.client;

import com.mrcrayfish.goblintraders.client.renderer.entity.GoblinTraderRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import xyz.tytanium.ragnagoblin.init.ModEntities;

@OnlyIn(Dist.CLIENT)
public class ClientHandler
{
    public static void setup()
    {
        RenderingRegistry.registerEntityRenderingHandler(ModEntities.RAGNA_GOBLIN.get(), GoblinTraderRenderer::new);
    }
}
