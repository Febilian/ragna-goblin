package xyz.tytanium.ragnagoblin;

import com.mrcrayfish.goblintraders.init.ModStats;
import com.mrcrayfish.goblintraders.trades.TradeManager;
import com.mrcrayfish.goblintraders.trades.type.BasicTrade;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import xyz.tytanium.ragnagoblin.client.ClientHandler;
import xyz.tytanium.ragnagoblin.init.ModEntities;
import xyz.tytanium.ragnagoblin.init.ModItems;
import xyz.tytanium.ragnagoblin.init.ModSounds;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("ragnagoblin")
public class RagnaGoblin
{

    public RagnaGoblin()
    {
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, Config.commonSpec);
        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        ModEntities.REGISTER.register(bus);
        ModItems.REGISTER.register(bus);
        ModSounds.REGISTER.register(bus);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onClientSetup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onCommonSetup);
    }

    private void onClientSetup(FMLClientSetupEvent event)
    {
        ClientHandler.setup();
    }

    private void onCommonSetup(FMLCommonSetupEvent event)
    {
        ModStats.init();
        ModEntities.registerEntityTypeAttributes();
        TradeManager manager = TradeManager.instance();
        manager.registerTrader(ModEntities.RAGNA_GOBLIN.get());
        manager.registerTypeSerializer(BasicTrade.SERIALIZER);
    }
}
