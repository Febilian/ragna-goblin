package xyz.tytanium.ragnagoblin.init;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import xyz.tytanium.ragnagoblin.Reference;
import xyz.tytanium.ragnagoblin.item.SupplierSpawnEggItem;

public class ModItems
{
    public  static final DeferredRegister<Item> REGISTER = DeferredRegister.create(ForgeRegistries.ITEMS, Reference.MOD_ID);

    public static final RegistryObject<Item> RAGNA_GOBLIN_SPAWN_EGG = REGISTER.register("ragna_goblin_spawn_egg", () -> new SupplierSpawnEggItem(ModEntities.RAGNA_GOBLIN::get, 0x000000, 0xFFFFFF, new Item.Properties().group(ItemGroup.MISC)));
}
