package xyz.tytanium.ragnagoblin.init;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import xyz.tytanium.ragnagoblin.Reference;

public class ModSounds
{
    public static final DeferredRegister<SoundEvent> REGISTER = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, Reference.MOD_ID);

    public static final RegistryObject<SoundEvent> ENTITY_RAGNA_GOBLIN_ANNOYED_GRUNT = build("entity.ragna_goblin.annoyed_grunt");
    public static final RegistryObject<SoundEvent> ENTITY_RAGNA_GOBLIN_IDLE_GRUNT = build("entity.ragna_goblin.idle_grunt");

    private static RegistryObject<SoundEvent> build(String id)
    {
        return REGISTER.register(id, () -> new SoundEvent(new ResourceLocation(Reference.MOD_ID, id)));
    }
}
