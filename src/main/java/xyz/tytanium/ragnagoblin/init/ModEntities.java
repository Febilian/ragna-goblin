package xyz.tytanium.ragnagoblin.init;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.attributes.GlobalEntityTypeAttributes;
import net.minecraft.world.World;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import xyz.tytanium.ragnagoblin.Reference;
import xyz.tytanium.ragnagoblin.entity.RagnaGoblinEntity;
import xyz.tytanium.ragnagoblin.item.SupplierSpawnEggItem;

import java.util.function.Function;

@Mod.EventBusSubscriber(modid = Reference.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModEntities
{
    public static final DeferredRegister<EntityType<?>> REGISTER = DeferredRegister.create(ForgeRegistries.ENTITIES, Reference.MOD_ID);

    public static final RegistryObject<EntityType<RagnaGoblinEntity>> RAGNA_GOBLIN = build("ragna_goblin", RagnaGoblinEntity::new, 0.5F, 1.0F);

    private static <T extends Entity> RegistryObject<EntityType<T>> build(String id, Function<World, T> function, float width, float height)
    {
        EntityType<T> type = EntityType.Builder.<T>create((entityType, world) -> function.apply(world), EntityClassification.CREATURE).size(width, height).setCustomClientFactory((spawnEntity, world) -> function.apply(world)).build(id.toString());
        return REGISTER.register(id, () -> type);
    }

    public static void registerEntityTypeAttributes()
    {
        GlobalEntityTypeAttributes.put(RAGNA_GOBLIN.get(), RagnaGoblinEntity.prepareAttributes().create());
    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public static void register(RegistryEvent.Register<EntityType<?>> event)
    {
        SupplierSpawnEggItem.updateEggMap();
    }
}
