package xyz.tytanium.ragnagoblin.entity;

import com.mrcrayfish.goblintraders.entity.AbstractGoblinEntity;
import com.mrcrayfish.goblintraders.trades.EntityTrades;
import com.mrcrayfish.goblintraders.trades.TradeManager;
import com.mrcrayfish.goblintraders.trades.TradeRarity;
import net.minecraft.entity.merchant.villager.VillagerTrades;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.MerchantOffers;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;
import xyz.tytanium.ragnagoblin.Config;
import xyz.tytanium.ragnagoblin.Reference;
import xyz.tytanium.ragnagoblin.init.ModEntities;
import xyz.tytanium.ragnagoblin.init.ModSounds;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

public class RagnaGoblinEntity extends AbstractGoblinEntity
{
    public RagnaGoblinEntity(World worldIn)
    {
        super(ModEntities.RAGNA_GOBLIN.get(), worldIn);
    }

    @Override
    public ResourceLocation getTexture()
    {
        return new ResourceLocation(Reference.MOD_ID, "textures/entity/ragna_goblin.png");
    }

    @Override
    protected void populateTradeData()
    {
        MerchantOffers offers = this.getOffers();
        EntityTrades entityTrades = TradeManager.instance().getTrades(ModEntities.RAGNA_GOBLIN.get());
        if(entityTrades != null)
        {
            Map<TradeRarity, List<VillagerTrades.ITrade>> tradeMap = entityTrades.getTradeMap();
            for(TradeRarity rarity : TradeRarity.values())
            {
                List<VillagerTrades.ITrade> trades = tradeMap.get(rarity);
                int min = rarity.getMaximum().apply(trades, this.rand);
                int max = rarity.getMaximum().apply(trades, this.rand);
                this.addTrades(offers, trades, Math.max(min, max), rarity.shouldShuffle());
            }
        }
    }

    @Override
    public ItemStack getFavouriteFood()
    {
        return new ItemStack(Items.POISONOUS_POTATO);
    }

    @Override
    public void livingTick()
    {
        super.livingTick();
        if(this.world.isRemote && this.ticksExisted % 2 == 0)
        {
            this.world.addParticle(ParticleTypes.DRAGON_BREATH, this.getPosX() - 0.5 + 1.0 * this.rand.nextDouble(), this.getPosY() + 0.5 - 0.5 + 1.0 * this.rand.nextDouble(), this.getPosZ() - 0.5 + 1.0 * this.rand.nextDouble(), 0, 0, 0);
        }
    }

    @Override
    public boolean isWaterSensitive()
    {
        return true;
    }

    @Override
    protected int getMaxRestockDelay()
    {
        return Config.COMMON.ragnaGoblin.restockDelay.get();
    }

    @Override
    public boolean canAttackBack()
    {
        return Config.COMMON.ragnaGoblin.canAttackBack.get();
    }

    @Nullable
    @Override
    protected SoundEvent getAmbientSound()
    {
        return ModSounds.ENTITY_RAGNA_GOBLIN_IDLE_GRUNT.get();
    }

    @Nullable
    @Override
    protected SoundEvent getHurtSound(DamageSource damageSourceIn)
    {
        return ModSounds.ENTITY_RAGNA_GOBLIN_ANNOYED_GRUNT.get();
    }

}
