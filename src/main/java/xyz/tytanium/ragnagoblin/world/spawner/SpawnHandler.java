package xyz.tytanium.ragnagoblin.world.spawner;

import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.DimensionType;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.server.FMLServerStartedEvent;
import net.minecraftforge.fml.event.server.FMLServerStoppedEvent;
import xyz.tytanium.ragnagoblin.Config;
import xyz.tytanium.ragnagoblin.Reference;
import xyz.tytanium.ragnagoblin.init.ModEntities;

import java.util.HashMap;
import java.util.Map;

@Mod.EventBusSubscriber(modid = Reference.MOD_ID)
public class SpawnHandler
{
    private static Map<ResourceLocation, RagnaGoblinSpawner> spawners = new HashMap<>();

    @SubscribeEvent
    public static void onServerStart(FMLServerStartedEvent event)
    {
        MinecraftServer server = event.getServer();
        spawners.put(DimensionType.THE_END_ID, new RagnaGoblinSpawner(server, "GoblinTrader", ModEntities.RAGNA_GOBLIN.get(), Config.COMMON.ragnaGoblin));
    }

    @SubscribeEvent
    public static void onServerStart(FMLServerStoppedEvent event)
    {
        spawners.clear();
    }

    @SubscribeEvent
    public static void onWorldTick(TickEvent.WorldTickEvent event)
    {
        if(event.phase != TickEvent.Phase.START)
            return;

        if(event.side != LogicalSide.SERVER)
            return;

        RagnaGoblinSpawner spawner = spawners.get(event.world.getDimensionKey().getLocation());
        if(spawner != null)
        {
            spawner.tick(event.world);
        }
    }
}
